from rest_framework import serializers
from blocks.talks.models import Talk
from blocks.location.models import Location
from blocks.sponsors.models import Sponsor


class TagListSerializer(serializers.WritableField):

    def from_native(self, data):
        if type(data) is not list:
            raise ParseError("expected a list of data")
        return data

    def to_native(self, obj):
        if type(obj) is not list:
            return [tag.name for tag in obj.all()]
        return obj


class TalkSerializer(serializers.ModelSerializer):
    tags = TagListSerializer()

    class Meta:
        model = Talk


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location


class SponsorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sponsor