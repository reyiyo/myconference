from django.utils.translation import ugettext_lazy as _


BLOCK_PRIVATE_TITLE = _("Private Conference Site")
BLOCK_PRIVATE_TEXT = _(
    "When \"Private Conference Site\" is activated, only "
    "members of the Conference can access to the conferente site.")
BLOCK_SUBMIT_TALK_TITLE = _("Submit Talk")
BLOCK_SUBMIT_TALK_TEXT = _(
    "When \"Submit Talk\" is activated, the registered attendees to the "
    "conference can propose talks for this event.")
BLOCK_ATTENDEES_TITLE = _("Show Attendees List")
BLOCK_ATTENDEES_TEXT = _(
    "Show the list of the people registered to the event in the "
    "conference site.")
BLOCK_SCHEDULE_TITLE = _("Schedule")
BLOCK_SCHEDULE_TEXT = _(
    "When \"Schedule\" is activated, a table with the conference agenda "
    "will be available in the conference site, and you will be able to arrange "
    "the talks in the proper slots in the Dashboard.")
BLOCK_MY_TALKS_TITLE = _("Show My Talks")
BLOCK_MY_TALKS_TEXT = _(
    "When \"My Talks\" is activated, the people registered in the conference "
    "will be able to see and manage their proposed talks.")
BLOCK_TALK_PAGE_TITLE = _("Talk Page")
BLOCK_TALK_PAGE_TEXT = _(
    "When \"Talk Page\" is activated, the people registered in the conference "
    "will be able to see an special page for each talk, where you can "
    "upload the video and material of the talk (most common use to be "
    "activated after the conference).")
BLOCK_CALL_FOR_PROPOSALS_TITLE = _("Call for Proposals")
BLOCK_CALL_FOR_PROPOSALS_TEXT = _(
    "When \"Call for Proposals\" is activated, you will be able to write "
    "a custom text about your Call for Proposals that will be published in "
    "the conference site.")
BLOCK_REVIEW_TALKS_TITLE = _("Review Talks")
BLOCK_REVIEW_TALKS_TEXT = _(
    "When \"Review Talks\" is activated, the organizers will be able to access "
    "a special section in the conference site to review and vote proposed "
    "talks.")
BLOCK_SPONSORS_TITLE = _("Sponsors")
BLOCK_SPONSORS_TEXT = _(
    "When \"Sponsors\" is activated, you will be able to show logos of your "
    "Sponsors in the conference site.")
BLOCK_INFORMATION_TITLE = _("Information")
BLOCK_INFORMATION_TEXT = _(
    "When \"Information\" is activated, your attendees will be able to see "
    "a page with relevant information about how to get to the venues.")
BLOCK_REGISTER_TITLE = _("Register Attendees")
BLOCK_REGISTER_TEXT = _(
    "When \"Register Attendees\" is activated, your conference will support "
    "registration.")
BLOCK_CODE_OF_CONDUCT_TITLE = _("Code of Conduct")
BLOCK_CODE_OF_CONDUCT_TEXT = _(
    "When \"Code of Conduct\" is activated, your will be able to write a "
    "document with the code of conducts of your conference and publish it in "
    "the conference site.")
BLOCK_PRESS_RELEASE_TITLE = _("Press Release")
BLOCK_PRESS_RELEASE_TEXT = _(
    "When \"Press Release\" is activated, your conference site will have a "
    "blog like section, where you will be able to publish news about the"
    "conference.")
BLOCK_VOLUNTEERS_TITLE = _("Show Volunteers")
BLOCK_VOLUNTEERS_TEXT = _(
    "When \"Show Volunteers\" is activated, a page with showing the people "
    "behind the conference will be available in the conference site.")
BLOCK_CONTACT_TITLE = _("Contact Form")
BLOCK_CONTACT_TEXT = _(
    "When \"Contact Form\" is activated, a page with a contact form will be "
    "available in the conference site to let people contact you if needed.")


def get_configurations(event):
    conference_blocks = [
        {"block_key": "block_submit_talk",
         "block_title": BLOCK_SUBMIT_TALK_TITLE,
         "block_text": BLOCK_SUBMIT_TALK_TEXT,
         "block_value": (
             "checked" if event.configuration.block_submit_talk else "")},
        {"block_key": "block_attendees",
         "block_title": BLOCK_ATTENDEES_TITLE,
         "block_text": BLOCK_ATTENDEES_TEXT,
         "block_value": (
             "checked" if event.configuration.block_attendees else "")},
        {"block_key": "block_schedule",
         "block_title": BLOCK_SCHEDULE_TITLE,
         "block_text": BLOCK_SCHEDULE_TEXT,
         "block_value": (
             "checked" if event.configuration.block_schedule else "")},
        {"block_key": "block_my_talks",
         "block_title": BLOCK_MY_TALKS_TITLE,
         "block_text": BLOCK_MY_TALKS_TEXT,
         "block_value": (
             "checked" if event.configuration.block_my_talks else "")},
        {"block_key": "block_talk_page",
         "block_title": BLOCK_TALK_PAGE_TITLE,
         "block_text": BLOCK_TALK_PAGE_TEXT,
         "block_value": (
             "checked" if event.configuration.block_talk_page else "")},
        {"block_key": "block_call_for_proposals",
         "block_title": BLOCK_CALL_FOR_PROPOSALS_TITLE,
         "block_text": BLOCK_CALL_FOR_PROPOSALS_TEXT,
         "block_value": (
             "checked"
             if event.configuration.block_call_for_proposals else "")},
        {"block_key": "block_sponsors",
         "block_title": BLOCK_SPONSORS_TITLE,
         "block_text": BLOCK_SPONSORS_TEXT,
         "block_value": (
             "checked" if event.configuration.block_sponsors else "")},
        {"block_key": "block_information",
         "block_title": BLOCK_INFORMATION_TITLE,
         "block_text": BLOCK_INFORMATION_TEXT,
         "block_value": (
             "checked" if event.configuration.block_information else "")},
        {"block_key": "block_register",
         "block_title": BLOCK_REGISTER_TITLE,
         "block_text": BLOCK_REGISTER_TEXT,
         "block_value": (
             "checked" if event.configuration.block_register else "")},
        {"block_key": "block_code_of_conduct",
         "block_title": BLOCK_CODE_OF_CONDUCT_TITLE,
         "block_text": BLOCK_CODE_OF_CONDUCT_TEXT,
         "block_value": (
             "checked" if event.configuration.block_code_of_conduct else "")},
        {"block_key": "block_press_release",
         "block_title": BLOCK_PRESS_RELEASE_TITLE,
         "block_text": BLOCK_PRESS_RELEASE_TEXT,
         "block_value": (
             "checked" if event.configuration.block_press_release else "")},
        {"block_key": "block_volunteers",
         "block_title": BLOCK_VOLUNTEERS_TITLE,
         "block_text": BLOCK_VOLUNTEERS_TEXT,
         "block_value": (
             "checked" if event.configuration.block_volunteers else "")},
        {"block_key": "block_contact",
         "block_title": BLOCK_CONTACT_TITLE,
         "block_text": BLOCK_CONTACT_TEXT,
         "block_value": (
             "checked" if event.configuration.block_contact else "")},
    ]

    organizers_blocks = [
        {"block_key": "private",
         "block_title": BLOCK_PRIVATE_TITLE,
         "block_text": BLOCK_PRIVATE_TEXT,
         "block_value": "checked" if event.configuration.private else ""},
        {"block_key": "block_review_talks",
         "block_title": BLOCK_REVIEW_TALKS_TITLE,
         "block_text": BLOCK_REVIEW_TALKS_TEXT,
         "block_value": (
             "checked" if event.configuration.block_review_talks else "")},
    ]

    configuration = {
        "first_time": event.configuration.first_time,
        "organizers_blocks": organizers_blocks,
        "conference_blocks": conference_blocks,
    }
    return configuration


def get_default_configuration():
    organizers_blocks = [
        {"block_key": "private",
         "block_title": BLOCK_PRIVATE_TITLE,
         "block_default": ""},
        {"block_key": "block_review_talks",
         "block_title": BLOCK_REVIEW_TALKS_TITLE,
         "block_default": "checked"},
    ]

    conference_blocks = [
        {"block_key": "block_submit_talk",
         "block_title": BLOCK_SUBMIT_TALK_TITLE,
         "block_default": "checked"},
        {"block_key": "block_attendees",
         "block_title": BLOCK_ATTENDEES_TITLE,
         "block_default": "checked"},
        {"block_key": "block_schedule",
         "block_title": BLOCK_SCHEDULE_TITLE,
         "block_default": "checked"},
        {"block_key": "block_my_talks",
         "block_title": BLOCK_MY_TALKS_TITLE,
         "block_default": "checked"},
        {"block_key": "block_talk_page",
         "block_title": BLOCK_TALK_PAGE_TITLE,
         "block_default": ""},
        {"block_key": "block_call_for_proposals",
         "block_title": BLOCK_CALL_FOR_PROPOSALS_TITLE,
         "block_default": ""},
        {"block_key": "block_sponsors",
         "block_title": BLOCK_SPONSORS_TITLE,
         "block_default": "checked"},
        {"block_key": "block_information",
         "block_title": BLOCK_INFORMATION_TITLE,
         "block_default": "checked"},
        {"block_key": "block_register",
         "block_title": BLOCK_REGISTER_TITLE,
         "block_default": "checked"},
        {"block_key": "block_code_of_conduct",
         "block_title": BLOCK_CODE_OF_CONDUCT_TITLE,
         "block_default": ""},
        {"block_key": "block_press_release",
         "block_title": BLOCK_PRESS_RELEASE_TITLE,
         "block_default": ""},
        {"block_key": "block_volunteers",
         "block_title": BLOCK_VOLUNTEERS_TITLE,
         "block_default": "checked"},
        {"block_key": "block_contact",
         "block_title": BLOCK_CONTACT_TITLE,
         "block_default": ""},
    ]

    configuration = {
        "organizers_blocks": organizers_blocks,
        "conference_blocks": conference_blocks,
    }
    return configuration