# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from eventmanager.models import Event, Configuration
from blocks.attendees.models import Profile
from blocks.talks.models import Talk
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Div, Layout, Field

# Forcing first name and last name input in signup form


class SignupForm(forms.Form):
    first_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(attrs={'placeholder': 'First name'}))
    last_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(attrs={'placeholder': 'Last name'}))

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit(
            'submit', 'Submit',
            css_class="btn btn-block btn-lg btn-danger"))

    def save(self, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', ]

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                'first_name',
                'last_name',
                css_class='column col-md-6 span5'
            ),
        )


class ProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div('country'),
                Div('state'),
                Div('phone'),
                css_class='column col-md-6 span5'
            ),
            Div(
                Div('personal_page'),
                Div('company'),
                Div('company_page'),
                css_class='column col-md-6 span5'
            ),
            Div(
                Div('cv_link'),
                Div('cv'),
                css_class='column col-md-6 span5'
            ),
            Div(
                Div('biography'),
                css_class='column col-md-12'
            ),
            Div(
                Div('show_email'),
                Div('show_real_name'),
                Div('public'),
                css_class='column col-md-6 span5'
            ),
            Div(
                Div('in_attendees'),
                Div('allow_contact'),
                css_class='column col-md-6 span5'
            ),
        )

        self.helper.add_input(Submit('submit', _('Submit')))

    class Meta:
        model = Profile
        exclude = ('user',)


class TalkForm(forms.ModelForm):
    #saved_talks = forms.ModelChoiceField(label=_('Saved talk'),
                                         #queryset=Talk.objects.all(),
                                         #required=False)

    def __init__(self, *args, **kwargs):
        #user = kwargs.pop('user', None)
        super(TalkForm, self).__init__(*args, **kwargs)
        #if user:
            #self.fields['saved_talks'].queryset = Talk.objects.filter(
                #speaker=user)
        self.helper = FormHelper()
        self.helper.form_id = 'submit-talk-form'
        self.helper.add_input(Submit('submit', _('Submit')))

    class Meta:
        model = Talk
        exclude = ('speaker',)
        #fields = ['saved_talks', 'title', 'summary',
                  #'tags', 'level', 'knowledge', 'notes']
        fields = ['title', 'summary',
                  'tags', 'level', 'knowledge', 'notes']


class EventSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EventSettingsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout('title', 'url', 'logo',
                                    'tags', 'summary',
                                    Field('start_date', css_class='datetimepicker'),
                                    Field('end_date', css_class='datetimepicker'),
                                    'timezone', 'language')

    class Meta:
        model = Event
        exclude = ('admin', 'members', 'configuration', 'reviewers')

    def clean(self):
        cleaned_data = super(EventSettingsForm, self).clean()
        start = cleaned_data.get('start_date')
        end = cleaned_data.get('end_date')
        if start and end:
            valid = end >= start
            if not valid:
                raise forms.ValidationError(_("End date can not be earlier than start date."))
        return cleaned_data


class ConfigurationForm(forms.ModelForm):
    class Meta:
        model = Configuration
        exclude = ('first_time', 'publish_talk_results', 'site_template')
