# -*- coding: utf-8 -*-
from django.contrib import admin

from eventmanager.models import Event, Configuration, Mail, Tasks
from blocks.attendees.models import Attendee, Profile
from blocks.custom_design.models import SectionsOrder
from blocks.location.models import Location
from blocks.sponsors.models import Sponsor
from blocks.talks.models import Talk, TalkEvent, ReviewMessage, TalkVote, TalkReview
from blocks.social.models import Social
from blocks.schedule.models import Track, Schedule
from blocks.sponsors.models import SponsorshipInfo
from blocks.single_pages.models import SinglePage


class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'url', 'tags', 'summary',
                    'start_date', 'end_date', 'configuration')
    list_filter = ('id', 'title', 'url', 'tags', 'summary',
                   'start_date', 'end_date', 'configuration')


class TalkAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'summary', 'speaker', 'tags', 'level',
                    'knowledge', 'notes')
    list_filter = ('id', 'title', 'summary', 'speaker', 'tags', 'level',
                   'knowledge', 'notes')


class TalkEventAdmin(admin.ModelAdmin):
    list_display = ('id', 'talk', 'event', 'video_url', 'state')
    list_filter = ('id', 'talk', 'event', 'video_url', 'state')


class TalkVoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'accepted')
    list_filter = ('id', 'user', 'accepted')


class ReviewMessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'date', 'body')
    list_filter = ('id', 'user', 'date', 'body')


class ConfigurationAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_time', 'publish_talk_results', 'private',
                    'site_template')
    list_filter = ('id', 'first_time', 'publish_talk_results', 'private',
                   'site_template')


class AttendeeAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile', 'event')
    list_filter = ('id', 'profile', 'event')


class MailAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'date', 'subject', 'body')
    list_filter = ('id', 'user', 'date', 'subject', 'body')


class SinglePageAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'category', 'text')
    list_filter = ('id', 'event', 'category', 'text')


class SocialAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'twitter_link', 'facebook_link',
                    'google_link', 'pinterest_link')
    list_filter = ('id', 'event', 'twitter_link', 'facebook_link',
                   'google_link', 'pinterest_link')


class TasksAdmin(admin.ModelAdmin):
    list_display = ('id', 'creator', 'event', 'title', 'priority',
                    'last_update', 'active', 'assigned', 'ending',
                    'closed_by')
    list_filter = ('id', 'creator', 'event', 'title', 'priority',
                   'last_update', 'active', 'assigned', 'ending',
                   'closed_by')


class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'address_lat', 'address_lng',
                    'more_info', 'smoking_allowed', 'eating_allowed',
                    'drinking_allowed', 'toilets')
    list_filter = ('id', 'event', 'address_lat', 'address_lng',
                   'more_info', 'smoking_allowed',
                   'eating_allowed', 'drinking_allowed', 'toilets')


class SectionsOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'order')
    list_filter = ('id', 'event', 'order')


class TrackAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'name')
    list_filter = ('id', 'event', 'name')


class SponsorshipInfoAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'url')
    list_filter = ('id', 'event', 'url')


class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'track', 'talk', 'activity_name',
                    'start_time', 'end_time', 'is_talk')
    list_filter = ('id', 'event', 'track', 'talk', 'activity_name',
                   'start_time', 'end_time', 'is_talk')


admin.site.register(Event, EventAdmin)
admin.site.register(Configuration, ConfigurationAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Mail, MailAdmin)
admin.site.register(Tasks, TasksAdmin)
admin.site.register(Attendee, AttendeeAdmin)
admin.site.register(Talk, TalkAdmin)
admin.site.register(TalkEvent, TalkEventAdmin)
admin.site.register(ReviewMessage, ReviewMessageAdmin)
admin.site.register(TalkVote, TalkVoteAdmin)
admin.site.register(Social, SocialAdmin)
admin.site.register(SinglePage, SinglePageAdmin)
admin.site.register(SectionsOrder, SectionsOrderAdmin)
admin.site.register(Track, TrackAdmin)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(SponsorshipInfo, SponsorshipInfoAdmin)
admin.site.register(Profile)
admin.site.register(Sponsor)
