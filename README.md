PyConference
============

Python/Django web service to manage everything related to a conference.


Website
-------

This project is still on development, but soon will be available at:
[http://pyconference.org|pyconference.org]


Get your own copy of the site (for development or learning):
------------------------------------------------------------

    # clone the thing
    git clone https://github.com/PyConference/PyConference.git

    # create a virtualenv for it
    cd PyConference
    mkvirtualenv pyconference

    # install all requirements
    pip install -r setup/requirements.txt

    # install redis-server
    # On Mac OS: brew install redis

    # install node.js (http://nodejs.org/) and dependencies
    # (For real-time notifications, you could use PyConference without this)
    npm install socket.io
    npm install cookie
    npm install redis

    # prepare it
    ./manage.py syncdb
    ./manage.py runserver

If you want to activate real-time notifications, you need to execute redis-server and the pyconference server with node.js like this:

    # In a different terminal:
    redis-server

    # In a different terminal:
    cd nodejs
    node pyconference.js
