from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from pyconference.common.utils import render_response
from eventmanager.api.api_views import is_valid_event_and_info
from blocks.sponsors.models import Sponsor, SponsorLevel
from blocks.sponsors.forms import SponsorForm, SponsorLevelForm

@login_required
@is_valid_event_and_info
def sponsors(request, eventid="0", **kw):
    """Sponsors manager"""
    event = kw["event"]
    sponsorform_errors = ''
    sponsorlevelform_errors = ''
    sponsor_level_form = SponsorLevelForm()
    sponsor_form = SponsorForm()
    if request.method == 'POST':
        post_data = request.POST
        if 'sponsor_submit' in post_data:
            form = SponsorForm(post_data, request.FILES)
            if form.is_valid():
                sponsor = form.save(commit=False)
                sponsor.event = event
                sponsor.save()
            else:
                sponsor_form = form
        elif 'sponsorlevel_submit' in post_data:
            form = SponsorLevelForm(post_data)
            if form.is_valid():
                level = form.save(commit=False)
                level.event = event
                level.save()
            else:
                sponsor_level_form = form

    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    sponsors = event.sponsor_set.all()
    sponsor_levels = event.sponsorlevel_set.all()
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'sponsors': sponsors,
        'sponsor_levels': sponsor_levels,
        'eventid': eventid,
        'sponsor_form': sponsor_form,
        'sponsor_level_form': sponsor_level_form}
    return render_response(request, 'sponsors.html', data)


@login_required
@is_valid_event_and_info
def edit_sponsor(request, eventid=0, **kw):
    event = kw["event"]
    sponsor_id = kw['sponsor_id']
    s = Sponsor.objects.get(id=sponsor_id, event=event)
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'eventid': eventid,
        'sponsor_id': sponsor_id,
        'messages': messages,
        'newer_tasks': newer_tasks
    }
    if request.method == 'POST':
        form = SponsorForm(request.POST, request.POST, instance=s)
        if form.is_valid:
            form.save()

        return HttpResponseRedirect(
            reverse('sponsors', args=[event.id]))
    else:
        form = SponsorForm(instance=s)
        data['form'] = form
        return render_response(request, 'edit_sponsor.html', data)


@login_required
@is_valid_event_and_info
def edit_sponsor_level(request, eventid=0, **kw):
    event = kw["event"]
    sponsor_level_id = kw['sponsor_level_id']
    s = SponsorLevel.objects.get(id=sponsor_level_id, event=event)
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'eventid': eventid,
        'sponsor_level_id': sponsor_level_id,
        'messages': messages,
        'newer_tasks': newer_tasks
    }
    if request.method == 'POST':
        form = SponsorLevelForm(request.POST, request.POST, instance=s)
        if form.is_valid:
            form.save()

        return HttpResponseRedirect(
            reverse('sponsors', args=[event.id]))
    else:
        form = SponsorLevelForm(instance=s)
        data['form'] = form
        return render_response(request, 'edit_sponsor_level.html', data)


@login_required
@is_valid_event_and_info
def delete_sponsor_level(request, eventid="0", **kw):
    event = kw['event']
    sponsor_level_id = kw['sponsor_level_id']
    is_admin = request.user in event.admin.all()
    if is_admin:
        sponsor_level = SponsorLevel.objects.get(
            id=sponsor_level_id, event=event)
        sponsor_level.delete()
    return HttpResponseRedirect(reverse('sponsors', args=[event.id]))


@login_required
@is_valid_event_and_info
def delete_sponsor(request, eventid="0", **kw):
    event = kw['event']
    sponsor_id = kw['sponsor_id']
    is_admin = request.user in event.admin.all()
    if is_admin:
        sponsor = Sponsor.objects.get(id=sponsor_id, event=event)
        sponsor.delete()
    return HttpResponseRedirect(reverse('sponsors', args=[event.id]))