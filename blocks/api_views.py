from rest_framework.decorators import api_view
from rest_framework.response import Response

from blocks.attendees.models import User


@api_view(['GET'])
def get_usernames(request, format=None):
    users = User.objects.values_list(
        "username", flat=True).order_by("username")
    return Response(users)