from django import forms
from .models import CustomDesign
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class DesignForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DesignForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', _('Submit')))

    class Meta:
        model = CustomDesign
        exclude = ('event',)