from django.conf.urls import patterns, url
from blocks.custom_design import views

urlpatterns = patterns(
    url(r'^design/(?P<eventid>\d+)/$', views.edit_design, name='locations'),
)