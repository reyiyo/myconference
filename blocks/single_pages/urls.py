from django.conf.urls import patterns, url

from blocks.single_pages import views


urlpatterns = patterns(
    '',

    # VIEWS
    url(r'^call_for_proposals/$', views.call_for_proposals,
        name='block_call_for_proposals'),
    url(r'^code_of_conduct/$', views.code_of_conduct,
        name='block_code_of_conduct'),
)
