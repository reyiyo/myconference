from django.http import Http404

from pyconference.common.utils import render_response
from blocks.single_pages.models import SinglePage
from blocks.views import event_context


@event_context
def call_for_proposals(request, url, **kw):
    if not kw["context"]["block_call_for_proposals"]:
        raise Http404
    context = kw["context"]
    event = kw["event"]
    context["conference_active"] = "active"
    category = "call_for_proposals"
    page = SinglePage.objects.filter(event=event, category=category).first()
    if page:
        context["page_content"] = page.text
    return render_response(request, 'single_page.html', context)


@event_context
def code_of_conduct(request, url, **kw):
    if not kw["context"]["block_code_of_conduct"]:
        raise Http404
    context = kw["context"]
    event = kw["event"]
    context["about_active"] = "active"
    category = "code_of_conduct"
    page = SinglePage.objects.filter(event=event, category=category).first()
    if page:
        context["page_content"] = page.text
    return render_response(request, 'single_page.html', context)
