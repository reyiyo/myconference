from django.conf.urls import patterns, url

from blocks.talks import views


urlpatterns = patterns(
    '',

    # VIEWS
    url(r'^my_talks/$', views.my_talks, name='block_my_talks'),
    url(r'^review_talks/$', views.review_talks, name='block_review_talks'),
    url(r'^review_talks/(?P<talkid>\d+)/$', views.talk_review,
        name='block_talk_review'),
    url(r'^talk_info/(?P<talkid>\d+)/$', views.talk_info,
        name='block_talk_info'),
    url(r'^edit_talk/(?P<talkid>\d+)/$', views.submit_talk,
        name='block_edit_talk'),
    url(r'^submit_talk/$', views.submit_talk, name='block_submit_talk'),
    url(r'^talks_summary/$', views.talks_summary, name='block_talks_summary'),
)
