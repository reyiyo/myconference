from django.http import Http404

from rest_framework.decorators import api_view
from rest_framework.response import Response
from taggit.models import Tag

from eventmanager.models import Event
from blocks.attendees.models import User
from blocks.talks.models import TalkEvent, TalkVote, TalkReview


@api_view(['GET'])
def remove_talk_event(request, talkid, format=None):
    talkid = int(talkid)
    talk = TalkEvent.objects.filter(
        talk__speaker=request.user, id=talkid).first()
    if talk:
        if not talk.event.configuration.block_submit_talk:
            raise Http404
        talk.delete()
    return Response({})


@api_view(['GET'])
def publish_talk_results(request, eventid, format=None):
    publish = request.GET.get('publish', "") == 'true'
    event = Event.objects.filter(id=int(eventid)).first()
    if event:
        if not event.configuration.block_submit_talk:
            raise Http404
        event.configuration.publish_talk_results = publish
        event.configuration.save()
    return Response({})


@api_view(['GET'])
def vote_talk(request, talkid, format=None):
    talkid = int(talkid)
    vote = int(request.GET.get('vote', "1")) > 0
    force_state = request.GET.get('force_state', False) == "true"
    talk = TalkEvent.objects.filter(id=talkid).first()
    if talk:
        is_admin = request.user in talk.event.admin.all()
        if not talk.event.configuration.block_submit_talk:
            raise Http404
        if is_admin and force_state:
            talk.accepted = vote
            talk.save()
            response = dict(valid=True)
            return Response(response)
        talk_vote, created = TalkVote.objects.get_or_create(
            user=request.user, talk_event=talk)
        talk_vote.accepted = vote
        talk_vote.save()
        review, _ = TalkReview.objects.get_or_create(talk_event=talk)
        if created:
            review.reviewers.add(request.user)
            review.votes.add(talk_vote)

        messages = review.messages.all().order_by("date")
        votes = review.votes.all()
        accepted_by = [votea.user for votea in votes if votea.accepted]
        rejected_by = [votea.user for votea in votes if not votea.accepted]
        user_votes = set(accepted_by + rejected_by + [talk.talk.speaker])
        pending = set([
            message.user
            for message in messages if message.user not in user_votes])
        if len(accepted_by) > len(rejected_by):
            talk.accepted = True
            talk.state = TalkEvent.UNDER_REVIEW
        elif len(rejected_by) >= len(accepted_by):
            talk.accepted = False
        talk.save()

        response = dict(valid=True)
        response["accepted_by"] = ", ".join(
            ["%s %s" % (user.first_name, user.last_name)
             for user in accepted_by])
        response["rejected_by"] = ", ".join(
            ["%s %s" % (user.first_name, user.last_name)
             for user in rejected_by])
        response["pending"] = ", ".join(
            ["%s %s" % (user.first_name, user.last_name)
             for user in pending])

        return Response(response)
    return Response({})


@api_view(['GET'])
def get_tags(request, format=None):
    tags = Tag.objects.all()
    if tags:
        return Response({"tags": [tag.name for tag in tags]})
    return Response({})


@api_view(['GET'])
def add_speaker(request, talkid, format=None):
    username = request.GET.get("username", "")
    user = User.objects.filter(username=username).first()
    talkid = int(talkid)
    talk = TalkEvent.objects.filter(
        talk__speaker=request.user, id=talkid).first()
    if user and talk and request.user == talk.talk.speaker:
        talk.talk.extra_speakers.add(user)
        talk.talk.save()
        data = {
            "username": user.username,
        }
        return Response(data)
    return Response({})