import datetime

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.utils import translation
from django.core.mail import EmailMessage

from pyconference.common.utils import render_response
from eventmanager.forms import TalkForm
from blocks.attendees.models import Profile
from blocks.talks.models import (
    Talk, TalkEvent, TalkReview, ReviewMessage
)
from blocks.views import event_context


@login_required
@event_context
def my_talks(request, url, **kw):
    if not kw["context"]["block_my_talks"]:
        raise Http404
    event = kw["event"]
    context = kw["context"]
    context["conference_active"] = "active"
    talks = TalkEvent.objects.filter(
        talk__speaker=request.user, event=event)
    context["talks"] = talks
    translation.activate(context['language'])
    return render_response(request, 'my_talks.html', context)


@login_required
@event_context
def talk_review(request, url, talkid, **kw):
    if not kw["context"]["block_review_talks"]:
        raise Http404
    event = kw["event"]
    context = kw["context"]
    context["organizers_active"] = "active"
    translation.activate(context['language'])
    talk = TalkEvent.objects.filter(id=talkid, event=event).first()
    if not context["is_organizer"] and talk.talk.speaker != request.user:
        raise Http404
    body = request.POST.get("body", "")
    if (request.method == 'GET' and talk) or body == "":
        context["talkid"] = talk.id
        context["talk"] = talk.talk
        levels = {
            Talk.BEGINNER: 'Beginner',
            Talk.INTERMEDIATE: 'Intermediate',
            Talk.ADVANCED: 'Advanced',
        }
        context["talk_level"] = levels[talk.talk.level]
        review, _ = TalkReview.objects.get_or_create(talk_event=talk)
        messages = review.messages.all().order_by("date")
        context["reviews"] = messages
        votes = review.votes.all()
        accepted_by = [vote.user for vote in votes if vote.accepted]
        rejected_by = [vote.user for vote in votes if not vote.accepted]
        user_votes = set(accepted_by + rejected_by + [talk.talk.speaker])
        pending = set(["%s %s" % (
            message.user.first_name, message.user.last_name)
            for message in messages if message.user not in user_votes])
        context["accepted_by"] = ", ".join(
            ["%s %s" % (user.first_name, user.last_name)
             for user in accepted_by])
        context["rejected_by"] = ", ".join(
            ["%s %s" % (user.first_name, user.last_name)
             for user in rejected_by])
        context["pending"] = ", ".join(pending)
        return render_response(request, 'talk_review.html', context)
    elif request.method == 'POST' and talk:
        user = request.user
        review, _ = TalkReview.objects.get_or_create(talk_event=talk)
        message = ReviewMessage(user=user, date=datetime.datetime.now(),
                                body=body)
        message.save()
        if review and message:
            review.messages.add(message)
            if user not in review.reviewers.all():
                review.reviewers.add(user)
            if talk.talk.speaker == request.user:
                talk.state = TalkEvent.ANSWERED
            else:
                talk.state = TalkEvent.NEEDS_REVIEW
            talk.save()
        if user != talk.talk.speaker:
            subject = "Comment added: [%s]" % talk.talk.title
            body_email = "Check out the comments at: %s" % reverse(
                "block_talk_review", args=[url, talkid])
            email = EmailMessage(subject, body_email, to=[
                talk.talk.speaker.email])
            email.send()
        else:
            review, _ = TalkReview.objects.get_or_create(talk_event=talk)
            messages = review.messages.all().order_by("date")
            filter_reviewers = set(
                [m.user.email for m in messages if m.user != user])
            subject = "Comment added: [%s]" % talk.talk.title
            body_email = "Check out the comments at: %s" % reverse(
                "block_talk_review", args=[url, talkid])
            email = EmailMessage(subject, body_email, to=filter_reviewers)
            email.send()

        return HttpResponseRedirect(reverse("block_talk_review",
                                    args=[url, talkid]))
    else:
        raise Http404


@login_required
@event_context
def review_talks(request, url, **kw):
    if not kw["context"]["block_review_talks"]:
        raise Http404
    event = kw["event"]
    context = kw["context"]
    translation.activate(context['language'])
    if not context["is_organizer"]:
        raise Http404
    context["organizers_active"] = "active"
    if context["publish_talk_results"]:
        context["publish_true"] = "checked"
    else:
        context["publish_false"] = "checked"
    talks_db = TalkEvent.objects.filter(event=event).order_by("talk__title")
    talks = []
    tags = set()
    talks_state_color = {
        "A": "funny-boxes-blue",
        "W": "funny-boxes-blue",
        "R": "funny-boxes-grey",
        "Default": "funny-boxes-red"
    }
    for talk in talks_db:
        tags_names = talk.talk.tags.names()
        review = TalkReview.objects.filter(talk_event=talk).first()
        extra_state = "%s" % str(talk.accepted)
        rejected_by = []
        if review:
            votes = review.votes.all()
            rejected_by = [votea.user for votea in votes if not votea.accepted]
            if review and request.user in review.reviewers.all():
                extra_state += " ME"
        if talk.accepted:
            talk_state = "A"
        elif len(rejected_by) > 0:
            talk_state = "R"
        else:
            talk_state = talk.state
        talks.append({
            "id": talk.id,
            "state": talk.state,
            "compose_state": "%s %s" % (talk.state, extra_state),
            "tags": " ".join(tags_names),
            "title": talk.talk.title,
            "author": "%s %s" % (talk.talk.speaker.first_name,
                                 talk.talk.speaker.last_name),
            "summary": talk.talk.summary,
            "talk_state_color": talks_state_color.get(
                talk_state, talks_state_color["Default"])
        })
        for tag in tags_names:
            tags.add(tag)
    context["talks"] = talks
    context["talks_count"] = len(talks)
    context["tags"] = sorted(tags)
    context["eventid"] = event.id
    context["is_admin"] = request.user in event.admin.all()
    return render_response(request, 'review_talks.html', context)


@login_required
@event_context
def talks_summary(request, url, **kw):
    if not kw["context"]["block_review_talks"]:
        raise Http404
    event = kw["event"]
    context = kw["context"]
    translation.activate(context['language'])
    if request.user not in event.admin.all():
        raise Http404
    context["organizers_active"] = "active"
    talks_db = TalkEvent.objects.filter(event=event).order_by("talk__title")
    talks = []
    for talk in talks_db:
        review = TalkReview.objects.filter(talk_event=talk).first()
        votes = review.votes.all()
        accepted = len([votea.user for votea in votes if votea.accepted])
        rejected = len([votea.user for votea in votes if not votea.accepted])
        talks.append({
            "talkid": talk.id,
            "approved": talk.accepted,
            "accepted": accepted,
            "rejected": rejected,
            "title": talk.talk.title,
            "author": "%s %s" % (talk.talk.speaker.first_name,
                                 talk.talk.speaker.last_name),
        })
    talks.sort(key=lambda x: (-x['accepted'], x['rejected']))
    context["talks"] = talks
    context["eventid"] = event.id
    return render_response(request, 'talks_revision_summary.html', context)


@event_context
def talk_info(request, url, **kw):
    #if not kw["context"]["block_talk_page"]:
        #raise Http404
    context = kw["context"]
    translation.activate(context['language'])
    talkid = int(kw["talkid"])
    context["conference_active"] = "active"
    talk = TalkEvent.objects.filter(id=talkid).first()
    if talk and talk.accepted:
        context["talk"] = talk
        profile = Profile.objects.filter(user=talk.talk.speaker).first()
        context["profile"] = profile
        return render_response(request, 'talk_info.html', context)
    else:
        raise Http404


@login_required
@event_context
def submit_talk(request, url, talkid=None, **kw):
    if not kw["context"]["block_submit_talk"]:
        raise Http404
    context = kw["context"]
    context["conference_active"] = "active"
    translation.activate(context['language'])

    if request.method == 'GET':
        if talkid is not None:
            event = kw["event"]
            talk = TalkEvent.objects.filter(id=talkid, event=event).first()
            form = TalkForm(instance=talk.talk)
        else:
            #form = TalkForm(user=request.user)
            form = TalkForm()
        context['talk_form'] = form
        return render_response(request, 'submit_talk.html', context)
    elif request.method == 'POST':
        event = kw["event"]
        if talkid is not None:
            saved_talk_id = int(talkid)
        else:
            saved_talk_id = request.POST.get('saved_talks', None)
            if saved_talk_id:
                saved_talk_id = int(saved_talk_id)
        if saved_talk_id:
            talkEvent = TalkEvent.objects.filter(
                id=saved_talk_id, event=event).first()
            talk = Talk.objects.get(speaker=request.user,
                                    id=talkEvent.talk.id)
            talk_form = TalkForm(request.POST, instance=talk)
        else:
            talk_form = TalkForm(request.POST)
            talk = talk_form.save(commit=False)
            talk.speaker = request.user
        talk_form.save()
        talk_event, _ = TalkEvent.objects.get_or_create(
            event=event, talk=talk)
        return HttpResponseRedirect(reverse('block_my_talks',
                                            args=[event.url]))