from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from taggit.managers import TaggableManager

from eventmanager.models import Event
from pyconference import settings


class Talk(models.Model):
    BEGINNER = 'B'
    INTERMEDIATE = 'I'
    ADVANCED = 'A'
    LEVEL_CHOICES = (
        (BEGINNER, _('Beginner')),
        (INTERMEDIATE, _('Intermediate')),
        (ADVANCED, _('Advanced')),
    )

    title = models.CharField(max_length=100, verbose_name=_('Title'))
    summary = models.TextField(verbose_name=_('Summary'))
    speaker = models.ForeignKey(User)
    extra_speakers = models.ManyToManyField(
        User, null=True, blank=True, related_name="speakers_user")
    tags = TaggableManager()
    level = models.CharField(
        verbose_name=_('Level'), max_length=1,
        choices=LEVEL_CHOICES, default=INTERMEDIATE)
    knowledge = models.TextField(
        verbose_name=_('Recommended background/knowledge'), null=True,
        blank=True)
    notes = models.TextField(verbose_name=_('Notes'), null=True, blank=True)

    class Meta:
        unique_together = ('speaker', 'title',)

    def __str__(self):
        return self.title


class TalkEvent(models.Model):
    PROPOSED = 'P'
    NEEDS_REVIEW = 'N'
    ANSWERED = 'W'
    UNDER_REVIEW = 'U'
    STATES = (
        (PROPOSED, _('Proposed')),
        (ANSWERED, _('Answered')),
        (NEEDS_REVIEW, _('Needs Review')),
        (UNDER_REVIEW, _('Under Review')),
    )
    talk = models.ForeignKey(Talk)
    event = models.ForeignKey(Event)
    video_url = models.CharField(max_length=200)
    material = models.FileField(upload_to=settings.MATERIAL_PATH)
    state = models.CharField(
        max_length=1, choices=STATES, default=PROPOSED)
    accepted = models.BooleanField(default=False)

    class Meta:
        unique_together = ('talk', 'event',)


class ReviewMessage(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField()
    body = models.TextField()


class TalkVote(models.Model):
    user = models.ForeignKey(User)
    talk_event = models.ForeignKey(
        TalkEvent, null=True, blank=True, default=None)
    accepted = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'talk_event',)


class TalkReview(models.Model):
    talk_event = models.ForeignKey(TalkEvent, unique=True)
    reviewers = models.ManyToManyField(User)
    messages = models.ManyToManyField(ReviewMessage)
    votes = models.ManyToManyField(TalkVote)


#lint:disable
@receiver(models.signals.post_delete, sender=Event)
def handle_deleted_event(sender, instance, **kwargs):
    if instance:
        talk_review = TalkReview.objects.filter(
            talk_event__event=instance).delete()
        talk_event = TalkEvent.objects.filter(event=instance).delete()
#lint:enable