# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Talk'
        db.create_table(u'talks_talk', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('summary', self.gf('django.db.models.fields.TextField')()),
            ('speaker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('level', self.gf('django.db.models.fields.CharField')(default='I', max_length=1)),
            ('knowledge', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('notes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'talks', ['Talk'])

        # Adding unique constraint on 'Talk', fields ['speaker', 'title']
        db.create_unique(u'talks_talk', ['speaker_id', 'title'])

        # Adding model 'TalkEvent'
        db.create_table(u'talks_talkevent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('talk', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['talks.Talk'])),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Event'])),
            ('video_url', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('material', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('state', self.gf('django.db.models.fields.CharField')(default='P', max_length=1)),
            ('accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'talks', ['TalkEvent'])

        # Adding unique constraint on 'TalkEvent', fields ['talk', 'event']
        db.create_unique(u'talks_talkevent', ['talk_id', 'event_id'])

        # Adding model 'ReviewMessage'
        db.create_table(u'talks_reviewmessage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'talks', ['ReviewMessage'])

        # Adding model 'TalkVote'
        db.create_table(u'talks_talkvote', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'talks', ['TalkVote'])

        # Adding model 'TalkReview'
        db.create_table(u'talks_talkreview', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('talk_event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['talks.TalkEvent'], unique=True)),
        ))
        db.send_create_signal(u'talks', ['TalkReview'])

        # Adding M2M table for field reviewers on 'TalkReview'
        m2m_table_name = db.shorten_name(u'talks_talkreview_reviewers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('talkreview', models.ForeignKey(orm[u'talks.talkreview'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['talkreview_id', 'user_id'])

        # Adding M2M table for field messages on 'TalkReview'
        m2m_table_name = db.shorten_name(u'talks_talkreview_messages')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('talkreview', models.ForeignKey(orm[u'talks.talkreview'], null=False)),
            ('reviewmessage', models.ForeignKey(orm[u'talks.reviewmessage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['talkreview_id', 'reviewmessage_id'])

        # Adding M2M table for field votes on 'TalkReview'
        m2m_table_name = db.shorten_name(u'talks_talkreview_votes')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('talkreview', models.ForeignKey(orm[u'talks.talkreview'], null=False)),
            ('talkvote', models.ForeignKey(orm[u'talks.talkvote'], null=False))
        ))
        db.create_unique(m2m_table_name, ['talkreview_id', 'talkvote_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'TalkEvent', fields ['talk', 'event']
        db.delete_unique(u'talks_talkevent', ['talk_id', 'event_id'])

        # Removing unique constraint on 'Talk', fields ['speaker', 'title']
        db.delete_unique(u'talks_talk', ['speaker_id', 'title'])

        # Deleting model 'Talk'
        db.delete_table(u'talks_talk')

        # Deleting model 'TalkEvent'
        db.delete_table(u'talks_talkevent')

        # Deleting model 'ReviewMessage'
        db.delete_table(u'talks_reviewmessage')

        # Deleting model 'TalkVote'
        db.delete_table(u'talks_talkvote')

        # Deleting model 'TalkReview'
        db.delete_table(u'talks_talkreview')

        # Removing M2M table for field reviewers on 'TalkReview'
        db.delete_table(db.shorten_name(u'talks_talkreview_reviewers'))

        # Removing M2M table for field messages on 'TalkReview'
        db.delete_table(db.shorten_name(u'talks_talkreview_messages'))

        # Removing M2M table for field votes on 'TalkReview'
        db.delete_table(db.shorten_name(u'talks_talkreview_votes'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'eventmanager.configuration': {
            'Meta': {'object_name': 'Configuration'},
            'block_attendees': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_call_for_proposals': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_code_of_conduct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_contact': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_information': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_my_talks': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_press_release': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_register': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_review_talks': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_sponsors': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_submit_talk': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_talk_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_volunteers': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'first_time': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'publish_talk_results': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'eventmanager.event': {
            'Meta': {'object_name': 'Event'},
            'admin': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_admin'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Configuration']"}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_member'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '1000'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        u'talks.reviewmessage': {
            'Meta': {'object_name': 'ReviewMessage'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'talks.talk': {
            'Meta': {'unique_together': "(('speaker', 'title'),)", 'object_name': 'Talk'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'knowledge': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.CharField', [], {'default': "'I'", 'max_length': '1'}),
            'notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'speaker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'summary': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'talks.talkevent': {
            'Meta': {'unique_together': "(('talk', 'event'),)", 'object_name': 'TalkEvent'},
            'accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'material': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'P'", 'max_length': '1'}),
            'talk': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['talks.Talk']"}),
            'video_url': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'talks.talkreview': {
            'Meta': {'object_name': 'TalkReview'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'messages': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['talks.ReviewMessage']", 'symmetrical': 'False'}),
            'reviewers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'symmetrical': 'False'}),
            'talk_event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['talks.TalkEvent']", 'unique': 'True'}),
            'votes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['talks.TalkVote']", 'symmetrical': 'False'})
        },
        u'talks.talkvote': {
            'Meta': {'object_name': 'TalkVote'},
            'accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['talks']