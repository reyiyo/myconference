from django.db import models
from django.contrib.auth.models import User

from eventmanager.models import Event
from blocks.talks.models import TalkEvent


class Track(models.Model):
    event = models.ForeignKey(Event)
    name = models.CharField(max_length=300)

    class Meta:
        unique_together = ('event', 'name',)


class Schedule(models.Model):
    event = models.ForeignKey(Event)
    track = models.ForeignKey(Track)
    talk = models.ForeignKey(TalkEvent, blank=True, null=True)
    activity_name = models.CharField(max_length=300, default="")
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    is_talk = models.BooleanField(default=True)


class ScheduleFavorites(models.Model):
    schedule = models.ForeignKey(Schedule)
    user = models.ForeignKey(User)

    class Meta:
        unique_together = ('schedule', 'user',)