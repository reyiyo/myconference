from django.conf.urls import patterns, url

from blocks.schedule import views


urlpatterns = patterns(
    '',

    # VIEWS
    url(r'^$', views.schedule, name='block_schedule'),
    url(r'^favorites/$', views.schedule_favorites,
        name='block_schedule_favorites'),
)
