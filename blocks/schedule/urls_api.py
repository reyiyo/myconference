from django.conf.urls import patterns, url

from blocks.schedule import api_views


urlpatterns = patterns(
    '',
    # API
    url(r'^add_favorite/(?P<scheduleid>\d+)/$', api_views.add_favorite),
    url(r'^remove_favorite/(?P<scheduleid>\d+)/$', api_views.remove_favorite),
)
