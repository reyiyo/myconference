from django.conf.urls import patterns, url

from blocks.register import views


urlpatterns = patterns(
    '',

    # VIEWS
    url(r'^$', views.register, name='block_register'),
)
