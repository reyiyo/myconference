from django.conf.urls import patterns, url

from blocks.attendees import views


urlpatterns = patterns(
    '',

    # VIEWS
    url(r'^(?P<username>((\w)+(\W)*)+)/$', views.profile, name='profile'),
    url(r'^$', views.attendees, name='block_attendees'),
)
