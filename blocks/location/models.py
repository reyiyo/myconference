from django.db import models
from django.utils.translation import ugettext_lazy as _

from eventmanager.models import Event


class Location(models.Model):
    event = models.ForeignKey(Event)
    name = models.CharField(max_length=300, default='')
    address_lat = models.CharField(max_length=100, default=0)
    address_lng = models.CharField(max_length=100, default=0)
    more_info = models.TextField(
        verbose_name=_('Additional information'), null=True, blank=True)
    smoking_allowed = models.BooleanField(
        verbose_name=_('Is smoking allowed?'), default=False)
    eating_allowed = models.BooleanField(verbose_name=_('Is eating allowed?'),
                                         default=False)
    drinking_allowed = models.BooleanField(
        verbose_name=_('Is drinking allowed?'), default=False)
    toilets = models.BooleanField(verbose_name=_('Are there public toilets?'),
                                  default=True)
    outlets = models.BooleanField(
        verbose_name=_('Are there available outlets?'), default=False)