from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from eventmanager.api.api_views import is_valid_event, is_valid_event_and_info
from blocks.location.models import Location
from blocks.location.forms import LocationForm
from pyconference.common.utils import render_response

@login_required
@is_valid_event_and_info
def locations(request, eventid="0", **kw):
    """Locations manager"""
    event = kw["event"]
    sponsorform_errors = ''
    sponsorlevelform_errors = ''
    if request.method == 'POST':
    	form = LocationForm(request.POST)
        if form.is_valid():
                loc = form.save(commit=False)
                loc.event = event
                loc.save()

    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    form = LocationForm()
    locations = event.location_set.all()
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'locations': locations,
        'eventid': eventid,
        'form': form}
    return render_response(request, 'locations.html', data)


@login_required
@is_valid_event_and_info
def edit_location(request, eventid=0, **kw):
    event = kw["event"]
    location_id = kw['location_id']
    l = get_object_or_404(Location, id=location_id, event=event)
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'eventid': eventid,
        'location_id': location_id,
        'messages': messages,
        'newer_tasks': newer_tasks
    }
    if request.method == 'POST':
        form = LocationForm(request.POST, request.POST, instance=l)
        if form.is_valid:
            form.save()

        return HttpResponseRedirect(
            reverse('locations', args=[event.id]))
    else:
        form = LocationForm(instance=l)
        data['form'] = form
        return render_response(request, 'edit_location.html', data)


@login_required
@is_valid_event_and_info
def delete_location(request, eventid="0", **kw):
    event = kw['event']
    location_id = kw['location_id']
    is_admin = request.user in event.admin.all()
    if is_admin:
        location = Location.objects.get(
            id=location_id, event=event)
        location.delete()
    return HttpResponseRedirect(reverse('locations', args=[event.id]))