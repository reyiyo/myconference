from django import forms
from blocks.location.models import Location
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class LocationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'location-form'
        self.helper.add_input(Submit('submit', _('Submit')))

    class Meta:
        model = Location
        exclude = ('event',)