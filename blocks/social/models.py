from django.db import models

from eventmanager.models import Event


class Social(models.Model):
    event = models.ForeignKey(Event)
    twitter_link = models.CharField(max_length=250)
    facebook_link = models.CharField(max_length=250)
    google_link = models.CharField(max_length=250)
    pinterest_link = models.CharField(max_length=250)
