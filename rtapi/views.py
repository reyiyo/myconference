import json

from eventmanager.models import User

from django.http import HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sessions.models import Session

import redis


#@csrf_exempt
#def node_api(request):
    #try:
        ##Get User from sessionid
        #session = Session.objects.get(session_key=request.POST.get('sessionid'))
        #user_id = session.get_decoded().get('_auth_user_id')
        #user = User.objects.get(id=user_id)

        ##Create comment
        #Comments.objects.create(user=user, text=request.POST.get('comment'))

        ##Once comment has been created post it to the chat channel
        #r = redis.StrictRedis(host='localhost', port=6379, db=0)
        #r.publish('chat', user.username + ': ' + request.POST.get('comment'))

        #return HttpResponse("Everything worked :)")
    #except Exception, e:
        #return HttpResponseServerError(str(e))


@csrf_exempt
def send_mail2(request):
    try:
        #Get User from sessionid
        #session = Session.objects.get(session_key=request.POST.get('sessionid'))
        #user_id = session.get_decoded().get('_auth_user_id')
        #user = User.objects.get(id=user_id)

        #Create comment
        #Comments.objects.create(user=user, text=request.POST.get('comment'))

        ##Once comment has been created post it to the chat channel
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        r.publish('notifications',
                  json.dumps({'title': 'Redis is cool!', 'author': 'haye321'}))

        return HttpResponse("true")
    except Exception, e:
        return HttpResponseServerError(str(e))