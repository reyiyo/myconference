git checkout .
if [ ! -d "pyconference/media" ]; then
  mkdir pyconference/media
fi
source /vol/myconference_env/bin/activate
git pull origin master

cp deploy/api_urls.js pyconference/static/js/api_urls.js
cp deploy/api_conference.js pyconference/static/js/conference/api_conference.js

./manage.py collectstatic

./manage.py syncdb --all

supervisorctl restart myconference
