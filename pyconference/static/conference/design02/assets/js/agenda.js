function addToFavorites(scheduleid) {
  $.get(buildUri("schedule", "add_favorite", scheduleid))
  .done(function(data) {
    if (data.added) {
      var $btnfavorite = $("#buttonFavorite" + scheduleid);
      $btnfavorite.attr("class", "likes btn-u btn-u-orange");
      $btnfavorite.attr("onclick", "removeFromFavorites(" + scheduleid + ")");
      $btnfavorite.html('<i class="fa fa-star"></i> Marked as favorite');
    }
  });
}

function removeFromFavorites(scheduleid) {
  $.get(buildUri("schedule", "remove_favorite", scheduleid))
  .done(function(data) {
    if (data.removed) {
      var $btnfavorite = $("#buttonFavorite" + scheduleid);
      $btnfavorite.attr("class", "likes btn-u btn-u-default");
      $btnfavorite.attr("onclick", "addToFavorites(" + scheduleid + ")");
      $btnfavorite.html('<i class="fa fa-star-o"></i> Add to favorites');
    }
  });
}

function showClassic() {
  $("#fullView").hide();
  $("#classicView").show();
}

function showFull() {
  $("#classicView").hide();
  $("#fullView").show();
}
