function saveChanges(category) {
  var eventid = $("#header").attr("name");
  var text = $("#txtDefaultHtmlArea").val();
  $.get(buildUri(eventid, "single_page", "save"),
  {category: category,
   text: text})
   .done(function (data){
     show_message("Page saved.");
   });
}
