var opts = {
  lines: 11, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};
var priority = "high";

function changePriority(value) {
  priority = value;
}

function changeCurrentUser(username) {
  $("#assignedName").text(username);
}

function showSpinner() {
  var div = document.getElementById('loading');
  var spinner = new Spinner(opts).spin(div);
}

function hideSpinner(page) {
  var div = $('#loading');
  div.hide();
}

function updateTask(taskid, date) {
  showSpinner();
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "task", "update", taskid),
  { year: date.getFullYear(),
    month: date.getMonth(),
    day: date.getDate()})
  .done(function( data ) {
    hideSpinner();
  });
}

$(function(){

    $('#external-events').find('div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()), // use the element's text as the event title
            id: $.trim($(this).find("i").attr("id"))
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },

        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            var $modal = $("#edit-modal"),
                $btn = $('#create-event');
            $btn.off('click');
            $btn.click(function () {
                var title = $("#taskName").val();
                var eventid = $("#header").attr("name");
                $("#taskTitleInput").val(title);
                $("#eventid").val(eventid);
                $("#assignedTo").val($("#assignedName").text());
                $("#priority").val(priority);
                $("#endingOn").val(start.getDate() + "/" + start.getMonth() + "/" + start.getFullYear());
                $.post(buildUri(eventid, "task", "create"), $('#createTask').serialize())
                .done(function(data) {
                  if (data.id || title) {
                      calendar.fullCalendar('addEventSource',
                          [{title: title, start: start, allDay: allDay, id: data.id}]
                      );
                  }
                  calendar.fullCalendar('unselect');
                });
            });
            $modal.find(".modal-title").html("Create Task");
            $modal.modal('show');
            calendar.fullCalendar('unselect');
        },
        editable: false,
        droppable:true,

        drop: function(date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            $(this).remove();
            updateTask(copiedEventObject.id, copiedEventObject.start);
        },

        eventClick: function(event) {
            // opens events in a popup window
            //TODO: let the user edit the event from here
            // if (event.url){
            //     window.open(event.url, 'gcalevent', 'width=700,height=600');
            //     return false
            // } else {
            //     var $modal = $("#edit-modal");
            //     $modal.find(".modal-title").html(event.title);
            //     $modal.find(".modal-body p").html(function(){
            //         if (event.allDay){
            //             return "All day event"
            //         } else {
            //             return "Start At: <strong>" + event.start.getHours() + ":" + (event.start.getMinutes() == 0 ? "00" : event.start.getMinutes()) + "</strong></br>"
            //                 + (event.end == null ? "" : "End At: <strong>" + event.end.getHours() + ":" + (event.end.getMinutes() == 0 ? "00" : event.end.getMinutes()) + "</strong>")
            //         }
            //     }());
            //     $modal.modal('show');
            // }
        }

    });

    $("#calendar-switcher").find("label").click(function(){
        calendar.fullCalendar( 'changeView', $(this).find('input').val() )
    });
    $("#today").click(function(){
        calendar.fullCalendar('today');
    });

    showSpinner();
    var eventid = $("#header").attr("name");
    $.get(buildUri(eventid, "task", "list", "dates"))
    .done(function( data ) {
      if (data) {
        for (var i = 0; i < data.length; i++) {
          var date = new Date(data[i].year, data[i].month, data[i].day);
          $('#calendar').fullCalendar('addEventSource',
            [{title: data[i].title, start: date, id: data[i].id}]);
        }
      }
      hideSpinner();
    });
});
