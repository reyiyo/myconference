
function setSocial(type) {
  var eventid = $("#header").attr("name");
  var link = $("#" + type + "_link").val();
  $.get(buildUri(eventid, "social", "set"),
  {type: type,
   link: link})
   .done(function (data){
     show_message("Social account updated.");
   });
}
