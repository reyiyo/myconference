var current_page = 0;

function updateMailListOnNotification() {
  if (current_page == 0) {
    _loadPage(0);
  }
}

callback = updateMailListOnNotification;

function showMailsView() {
  hideSpinner();
  $("#composeView").hide();
  $("#readView").hide();
  $("#mailsView").show();
}

function showComposeView() {
  hideSpinner();
  $("#mailsView").hide();
  $("#readView").hide();
  $("#composeView").show();
}

function openMail(id, unreadMode) {
  showSpinner();
  $("#mailsView").hide();
  $("#composeView").hide();
  $("#readView").hide();
  // Load mail data
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "mail", "read", id))
  .done(function( data ) {
      if (data) {
        var $content = $("#mailRead").clone();
        $("#accordion").html("");
        $("#mailId").attr("value", data.mail_id);
        $("#readSubject").html("<i class='fa fa-envelope-o'></i> " + data.subject);
        if (data.mails.length) {
          if (data.mails[0].unread) {
            $content.find("#readUsername").html("<strong>" +
              data.mails[0].username + " (" + data.mails[0].date + ")</strong>");
          } else {
            $content.find("#readUsername").html(
              data.mails[0].username + " (" + data.mails[0].date + ")");
          }
          $content.find("#readBody").html(data.mails[0].body.replace(/\r?\n/g,'<br/>'));
        }
        $content.appendTo( "#accordion" );
        for (var i = 1; i < data.mails.length; i++) {
          var newContent = $content.clone();
          if (data.mails[i].unread) {
            newContent.find("#readUsername").html("<strong>" +
              data.mails[i].username + " (" + data.mails[i].date + ")</strong>");
            newContent.find("#readUsername").attr("class", "accordion-toggle");
            newContent.find("#collapseItem").attr("class", "panel-collapse in collapse");
          } else {
            newContent.find("#readUsername").html(
              data.mails[i].username + " (" + data.mails[i].date + ")");
            newContent.find("#readUsername").attr("class", "accordion-toggle collapsed");
            newContent.find("#collapseItem").attr("class", "panel-collapse collapse");
          }
          newContent.find("#readUsername").attr("href", "#collapseItem" + i);
          newContent.find("#collapseItem").attr("id", "collapseItem" + i);
          newContent.find("#readBody").html(data.mails[i].body);
          newContent.appendTo( "#accordion" );
        }
      }
      // Update counters
      if (unreadMode) {
        var mailItem = $("#mailItem" + id);
        mailItem.attr("onclick", "openMail(" + id + ", false);");
        mailItem.find("#envelope").attr("class", "fa fa-envelope-o");
        mailItem.find("#mailName").html(mailItem.find("#mailName").text());
        mailItem.find("#mailSubject").html(mailItem.find("#mailSubject").text());
        var value = parseInt($("#inboxCounter").text()) - 1;
        $("#inboxCounter").text(value);
        $("#inboxHeaderCounter").text(value);
        if (value == 0) {
          highlightWithColor("#messages", "#7f7574");
        }
        $("#unreadMessages").text("(" + value + " unread messages)");
      }
      $("#readView").show();
      hideSpinner();
  });
}

function reply() {
  $("#replyAll").hide();
  $("#messageReply").show();
  $("#replySend").show();
  $("#replyDiscard").show();
}

function discardReply() {
  $("#messageReply").hide();
  $("#replySend").hide();
  $("#replyDiscard").hide();
  $("#replyAll").show();
}

var opts = {
  lines: 11, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};

function showSpinner() {
  var div = document.getElementById('loading');
  var spinner = new Spinner(opts).spin(div);
}

function hideSpinner(page) {
  var div = $('#loading');
  div.hide();
}

function loadPage(previous) {
  showSpinner();
  page = current_page + 1
  if (previous) {
    page = current_page - 1;
  }
  _loadPage(page);
}

function _loadPage(page) {
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "mail", "page", page))
  .done(function( data ) {
      if (data.mails.length > 0) {
        $("#tableBody").html("");
        current_page = page;
        mails = data.mails;
        $(".total-pages > i").text(data.page + " of " + data.pagenum);
        for (var i = 0; i < mails.length; i++) {
          var unread = false;
          var envelope = "fa-envelope-o";
          var username = mails[i].username;
          var subject = mails[i].subject;
          var mailid = mails[i].id;
          var date = mails[i].date;
          if (mails[i].unread) {
            var unread = true;
            var envelope = "fa-envelope";
            var username = "<strong>" + mails[i].username + "</strong>";
            var subject = "<strong>" + mails[i].subject + "</strong>";
          }
          mailItem = '<tr onclick="openMail(' + mailid + ', ' + unread + ');" id="mailItem' + mailid + '">'
                    + '<td class="tiny-column"><i id="envelope" class="fa ' + envelope + '"></i></td>'
                    + '<td class="name" id="mailName">' + username + '</td>'
                    + '<td class="subject" id="mailSubject">' + subject + '</td>'
                    + '<td class="name">' + date + '</td>'
                    + '</tr>';
          $("#tableBody").append(mailItem);
        }
      }
      hideSpinner();
  });
}
