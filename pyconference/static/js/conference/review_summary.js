function voteTalk(vote, talkid) {
  $.get(buildUri("talks", talkid, "vote"),
  { vote: vote,
    force_state: true})
  .done(function(data) {
    if (data.valid) {
      var $talkTitle = $("#talkTitle" + talkid);
      if (vote > 0) {
        $talkTitle.attr("class", "fa fa-star");
      } else {
        $talkTitle.attr("class", "fa fa-star-o");
      }
    }
  });
}
