function voteTalk(vote, talkid) {
  $.get(buildUri("talks", talkid, "vote"),
  { vote: vote})
  .done(function(data) {
    if (data.valid) {
      $("#accepted").text(data.accepted_by);
      $("#rejected").text(data.rejected_by);
      $("#pending").text(data.pending);
    }
  });
}
