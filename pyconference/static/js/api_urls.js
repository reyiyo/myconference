API_DASHBOARD = "http://localhost:8000/dashboard/api/"

function buildUri() {
  uri = API_DASHBOARD;
  for (var i = 0; i < arguments.length; i++) {
    uri += arguments[i] + "/";
  }

  return uri;
}
