// Realtime Notifications
var socket = io.connect('localhost',
  {port: 4000, 'sync disconnect on unload': true});

var callback;
var callbackTasks;

socket.on('mails', function(data) {
  updateMessagesMenu(data);
  if (callback) {
    callback();
  }
});

socket.on('tasks', function(data) {
  updateTasksMenu(data);
  if (callbackTasks) {
    callbackTasks();
  }
});

function updateMessagesMenu(data) {
  var subject = $.parseJSON(data).subject;
  var user = $.parseJSON(data).user;
  var link = $.parseJSON(data).link;
  var eventid = $("#header").attr("name");

  $.get(buildUri(eventid, "mail", "unread_count"))
  .done(function( result ) {
    var $inboxCounter = $("#inboxCounter");
    if (result.unread && $inboxCounter) {
      $inboxCounter.text(result.unread);
      $("#inboxHeaderCounter").text(result.unread);
      var $titleUnread = $("#unreadMessages");
      var $managerMessagesCounter = $("#managerMessagesCounter");
      if ($titleUnread) {
        $titleUnread.text("(" + result.unread + " unread messages)");
      }
      if ($managerMessagesCounter) {
        $managerMessagesCounter.text(result.unread);
      }
      show_message("New message!");
    }
  });
  var $menu = $("#messages-menu");
  if ($menu) {
    $menu.find("li").slice(2,3).remove();
    var item = '<li role="presentation">'
             + '<a href="' + link + '" class="message">'
             + '<img src="img/1.jpg" alt="">'
             + '<div class="details">'
             + '<div class="sender">' + user + '</div>'
             + '<div class="text">'
             + subject
             + '</div>'
             + '</div>'
             + '</a>'
             + '</li>';
    $menu.prepend(item);
    highlight("#messages");
  }
}

function updateTasksMenu(data) {
  var priority = $.parseJSON(data).priority;
  var title = $.parseJSON(data).title;
  var link = $.parseJSON(data).link;
  var user = $.parseJSON(data).user;
  var eventid = $("#header").attr("name");

  $.get(buildUri(eventid, "task", "count"))
  .done(function( result ) {
    var $tasksCounter = $("#tasksCounter");
    if (result.pending_tasks && $tasksCounter) {
      $tasksCounter.text(result.pending_tasks);
      $("#tasksHeaderCounter").text(result.pending_tasks);
      var $everyoneCounter = $("#everyoneCounter");
      if ($everyoneCounter) {
        $everyoneCounter.text(result.pending_tasks);
      }
      var $managerTasksCounter = $("#managerTasksCounter");
      if ($managerTasksCounter) {
        $managerTasksCounter.text(result.pending_tasks);
      }
      show_message("New task!");
    }
  });
  var $menu = $("#tasks-menu");
  if ($menu) {
    $menu.find("li").slice(4,5).remove();
    var icon = '<span class="label label-important"><i class="fa fa-bell-o"></i></span>';
    if (priority == 2) {
      icon ='<span class="label label-warning"><i class="fa fa-exclamation-circle"></i></span>';
    } else if (priority == 3) {
      icon = '<span class="label label-success"><i class="fa fa-tag"></i></span>';
    }
    var item = '<li role="presentation">'
             + '<a href="' + link + '" class="support-ticket">'
             + '<div class="picture">'
             + icon
             + '</div>'
             + '<div class="details">'
             + title
             + '</div>'
             + '</a>'
             + '</li>';
    $menu.prepend(item);
    highlight("#tasks-menu");
  }
  var $feedsList = $("#feedsList");
  if ($feedsList) {
    var item = '<section class="feed-item">'
            + '<div class="icon pull-left">'
            + '<i class="fa fa-bookmark color-orange"></i>'
            + '</div><div class="feed-item-body"><div class="text">'
            + '<a href="#">' + user + '</a> has modified <a href="#">' + title + '</a>.'
            + '</div><div class="time pull-left">Now</div></div></section>';
    $feedsList.prepend(item);
  }
}

// Base Functions
function show_message(message) {
  var $notification = $("#notification");
  $notification.find("#notificationMessage").text(" " + message);
  $notification.show();
  var timer = window.setTimeout( function() {
    timer = null;
    $notification.hide();
  }, 3000 );
}

function highlight(itemId) {
  $(itemId).animate({color: '#ef4444'}, 1000 );
}

function highlightWithColor(itemId, colorName) {
  $(itemId).animate({color: colorName}, 1000 );
}
