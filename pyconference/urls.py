from django.conf.urls import patterns, include, url
from django.conf.urls import handler404, handler500
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from pyconference.common import views


urlpatterns = patterns(
    '',
    # Skip the logout-confirmation step
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout',
     {'next_page': '/'}),
    # Login
    (r'^accounts/', include('allauth.urls')),
    # url(r'^pyconference/', include('pyconference.foo.urls')),
    # APIs
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    # User Dashboard
    (r'^dashboard/', include('eventmanager.urls')),
    # Realtime API
    (r'^rtapi/', include('rtapi.urls')),
    # Resolve Conferences url
    url(r'^(?P<url>((\w)+(-(\w)+)*)+)/$', views.show_conference,
        name='show_conference'),
    url(r'^(?P<url>((\w)+(-(\w)+)*)+)/', include('blocks.urls')),
    url(r'^blocksapi/', include('blocks.urls_api')),
    # Home
    url(r'^$', views.home, name='home'),
)

handler404 = "pyconference.common.views.error_404"  # lint:ok
handler500 = "pyconference.common.views.error_404"  # lint:ok
